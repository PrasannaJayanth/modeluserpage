<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Details Form</title>
    <link rel="stylesheet" href="./assests/css/bootstrap.min.css">
    <style>
        .form-control:focus {
            box-shadow: none;

        }
    </style>
</head>

<body>

    <div class="container mt-5">
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
            Add User
        </button>
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="mb-3 mt-5 ">
                            <label for="formGroupEmail" class="form-label">Name</label>
                            <input type="text" class="form-control " id="formGroupName" placeholder="Enter your Name">
                        </div>
                        <div class="mb-3">
                            <label for="formGroupEmail" class="form-label">Email</label>
                            <input type="text" class="form-control " id="formGroupExampleInput2" placeholder="Enter your email">
                        </div>
                        <div class="mb-3">
                            <label for="formGroupMobile" class="form-label">Mobile Number</label>
                            <input type="number" class="form-control " id="formGroupmobilenumber" placeholder="Enter your mobile number">
                        </div>
                        <div class="mb-3">
                            <label for="formGroupPassword" class="form-label">Password</label>
                            <input type="password" class="form-control " id="formGroupPassword" placeholder="Enter your pasword">
                        </div>
                        <div class="mb-3">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    </div>
    <div class="table-responsive">
        <table class="container table mt-5 ">
            <thead>
                <tr>
                    <th scope="col">Sl.no</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Password</th>
                    <th scope="col">Operation</th>
                </tr>
            </thead>
            <tbody>
                <tr class="">
                    <th scope="row">1</th>
                    <td>Siva</td>
                    <td>siva123@gmail.com</td>
                    <td>6758943263</td>
                    <td>123@#siva</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                            Update
                        </button>
                        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="mb-3 mt-5 ">
                                            <label for="formGroupEmail" class="form-label">Name</label>
                                            <input type="text" class="form-control " id="formGroupName" placeholder="Enter your Name">
                                        </div>
                                        <div class="mb-3">
                                            <label for="formGroupEmail" class="form-label">Email</label>
                                            <input type="text" class="form-control " id="formGroupExampleInput2" placeholder="Enter your email">
                                        </div>
                                        <div class="mb-3">
                                            <label for="formGroupMobile" class="form-label">Mobile Number</label>
                                            <input type="number" class="form-control " id="formGroupmobilenumber" placeholder="Enter your mobile number">
                                        </div>
                                        <div class="mb-3">
                                            <label for="formGroupPassword" class="form-label">Password</label>
                                            <input type="password" class="form-control " id="formGroupPassword" placeholder="Enter your pasword">
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-danger ms-1"><a class="text-light text-decoration-none" href="#">Delete</a></button>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    </div>


    <script src="./assests/js/bootstrap.min.js"></script>
</body>

</html>